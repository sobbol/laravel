<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Brand;

use App\Exceptions\IncorectSaveDataException;
use App\Http\Resources\BrandResource;
use App\Models\Brands;
use ArgumentCountError;
use Cache;
use Exception;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class BrandService
{
    /**
     * @throws IncorectSaveDataException
     */
    public function createBrand($data): Response
    {
        try {
            Brands::query()->create($data->toArray());
            return response(['message' => 'brand created'], Response::HTTP_CREATED);
        } catch (Exception|ArgumentCountError $e) {
            Log::error('CREATE BRAND ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateBrand($data) : Response
    {
        try {
            Brands::query()->where('id', $data->id)->update($data->toArray());
            return response(['message' => 'brand updated'], Response::HTTP_OK);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('UPDATE BRAND ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    public function getBrandId(string $id): BrandResource
    {
        return Cache::remember('brand_' . $id, config('cache.lifetime'), function () use ($id) {
            return new BrandResource(Brands::findOrFail($id));
        });
    }

    public function getAllBrands(array $data): ResourceCollection
    {
        $perPage = $data['per_page'] ?? config('pagination.per_page');
        $page    = $data['page'] ?? 1;

        return Cache::remember('brands_' . $page, config('cache.lifetime'), function () use ($perPage) {
            return BrandResource::collection(Brands::desc()->paginate($perPage));
        });
    }
}
