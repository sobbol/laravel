<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Currency;

use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

/**
 * @property HttpClient client
 * @property mixed      apiKey
 */
class NationalBankService
{

    /**
     * NovaposhtaService constructor.
     */
    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    public function getCourses(): bool|string
    {
        return $this->client->send('GET', []);
    }

    public function getWarehouses(): bool|string
    {
        $params = [
            "apiKey"       => $this->apiKey,
            "modelName"    => "Address",
            "calledMethod" => "getWarehouses",
        ];

        return $this->client->send('POST', $params);
    }

    public function saveCourses(array $currencies)
    {

        foreach ($currencies as $key => $currency) {

            Currency::query()->updateOrInsert(
                [
                    'code' => $currency['cc'],
                ],
                [
                    'id'         => Str::uuid(),
                    'name'       => $currency['txt'],
                    'sign'       => null,
                    'code'       => $currency['cc'],
                    'rate_from'  => 1.00,
                    'rate_to'    => $currency['rate'],
                    'cents'      => 2,
                    'position'   => $key + 2,
                    'enabled'    => true,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),

                ]
            );


        }

    }

    public function saveWarehouses($warehouses)
    {
        foreach ($warehouses['data'] as $warehouse) {

            Warehouse::query()->updateOrInsert(
                [
                    'ref'         => $warehouse["Ref"],
                    "city_ref"    => $warehouse["CityRef"],
                    "description" => $warehouse["Description"],
                ],
                [
                    "id"                 => Str::uuid(),
                    "description"        => $warehouse["Description"],
                    "short_address"      => $warehouse["ShortAddress"],
                    "phone"              => $warehouse["Phone"],
                    "ref"                => $warehouse["Ref"],
                    "city_ref"           => $warehouse["CityRef"],
                    "city_description"   => $warehouse["CityDescription"],
                    "area"               => $warehouse["SettlementDescription"],
                    "area_description"   => $warehouse["SettlementAreaDescription"],
                    "area_type"          => $warehouse["SettlementTypeDescription"],
                    "region_description" => $warehouse["SettlementRegionsDescription"],
                    "longitude"          => $warehouse["Longitude"],
                    "latitude"           => $warehouse["Latitude"],
                    "created_at"         => Carbon::now(),
                    "updated_at"         => Carbon::now(),

                ]
            );


        }

    }
}
