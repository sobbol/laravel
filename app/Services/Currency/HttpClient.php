<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Currency;

class HttpClient
{
    public function send($method, $params): bool|string
    {
        $path   = config('banks.national_bank');
        $header = ['Accept: application/json', 'Content-Type: application/json'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
