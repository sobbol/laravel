<?php

namespace App\Console\Commands;

use App\Services\Currency\NationalBankService;
use Illuminate\Console\Command;

class CurrencyCourse extends Command
{

    protected $signature = 'currency:getCourse';

    protected $description = 'Отримання курсів валют з Національного Банку';

    public function handle(NationalBankService $nationalBankService)
    {
        $this->comment('Старт синхронізації курсів валют');
        $courses = json_decode($nationalBankService->getCourses(), true);
        if ($courses) $nationalBankService->saveCourses($courses);
        $this->comment('Кінець синхронізації курсів валют');
    }

}
