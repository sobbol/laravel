<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Exceptions;

use Exception;

class BadCredentialsException extends Exception
{
    protected $message = 'bad_credentials';
}
