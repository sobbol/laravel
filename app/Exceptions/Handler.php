<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;
use App\Traits\ErrorResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ErrorResponse;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $exception) {
            if (app()->bound('sentry') && $this->shouldReport($exception)) {
                app('sentry')->captureException($exception);
            }
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof EmailUnconfirmedException)
        {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof UserBlockedException)
        {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof NotFoundException)
        {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof BadCredentialsException)
        {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof IncorectSaveDataException)
        {
            return response($this->errorResponse($exception->getMessage()), Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof ModelNotFoundException)
        {
            return response(['error' => 'not_found', 'message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof NotFoundHttpException)
        {
            return response(['error' => 'not_found'], Response::HTTP_NOT_FOUND);
        }

        return parent::render($request, $exception);
    }
}
