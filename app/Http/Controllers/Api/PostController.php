<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Services\Post\PostService;
use App\Http\Resources\PostResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Posts\GetAllNewsRequest;
use App\Http\Requests\Posts\GetAllBlogsRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Exceptions\IncorectSaveDataException;

class PostController extends Controller
{
    private PostService $service;

    /**
     * PostController constructor.
     */
    public function __construct(PostService $postService)
    {
        $this->service = $postService;
    }

    public function getAllNews(GetAllNewsRequest $request): AnonymousResourceCollection
    {
        return $this->service->getAllNews($request->toArray());
    }

    public function getAllBlogs(GetAllBlogsRequest $request): AnonymousResourceCollection
    {
        return $this->service->getAllBlogs($request->toArray());
    }

    public function getNewsId($id): PostResource
    {
        return $this->service->getNewsId($id);
    }

    public function getBlogId($id): PostResource
    {
        return $this->service->getBlogId($id);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function createPost(CreatePostRequest $request): Response
    {
        return $this->service->createPost($request);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updatePost(UpdatePostRequest $request): Response
    {
        return $this->service->updatePost($request);
    }
}
