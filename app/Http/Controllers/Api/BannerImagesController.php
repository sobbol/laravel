<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannerImages\CreateBannerImagesRequest;
use App\Http\Requests\BannerImages\GetAllBannerImagesRequest;
use App\Http\Requests\BannerImages\UpdateBannerImagesRequest;
use App\Http\Resources\BannerImageResource;
use App\Services\BannerImages\BannerImagesService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Exceptions\IncorectSaveDataException;


class BannerImagesController extends Controller
{
    private BannerImagesService $service;

    /**
     * BannerController constructor.
     */
    public function __construct(BannerImagesService $bannerImagesService)
    {
        $this->service = $bannerImagesService;
    }

    public function getAllImages(GetAllBannerImagesRequest $request): AnonymousResourceCollection
    {
        return $this->service->getAllImages($request->toArray());
    }

    public function getImageId($id): BannerImageResource
    {
        return $this->service->getImageId($id);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function createImage(CreateBannerImagesRequest $request): Response
    {
        return $this->service->createBannerImage($request->toArray());
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateImage(UpdateBannerImagesRequest $request): Response
    {
        return $this->service->updateBannerImage($request->toArray());
    }
}

