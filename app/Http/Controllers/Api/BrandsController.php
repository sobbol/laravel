<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Exceptions\IncorectSaveDataException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Brands\CreateBrandRequest;
use App\Http\Requests\Brands\GetAllBrandsRequest;
use App\Http\Requests\Brands\UpdateBrandRequest;
use App\Http\Resources\BrandResource;
use App\Services\Brand\BrandService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class BrandsController extends Controller
{
 private BrandService $service;

    /**
     * BrandsController constructor.
     */
    public function __construct(BrandService $brandService)
    {
        $this->service = $brandService;
    }

    public function getAllBrands(GetAllBrandsRequest $request): ResourceCollection
    {
        return $this->service->getAllBrands($request->toArray());
    }

    public function getBrandId($id): BrandResource
    {
        return $this->service->getBrandId($id);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function createBrand(CreateBrandRequest $request): Response
    {
        return $this->service->createBrand($request);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateBrand(UpdateBrandRequest $request): Response
    {
        return $this->service->updateBrand($request);
    }
}
