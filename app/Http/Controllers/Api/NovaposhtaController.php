<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NovaPoshta\CityByNameRequest;
use App\Http\Requests\NovaPoshta\GetAllWarehousesRequest;
use App\Services\NovaPoshta\NovaposhtaService;
use Illuminate\Http\Resources\Json\ResourceCollection;

class NovaposhtaController extends Controller
{
    private NovaposhtaService $service;

    /**
     * NovaposhtaController constructor.
     */
    public function __construct(NovaposhtaService $novaposhtaService)
    {
        $this->service   = $novaposhtaService;
    }

    public function getAllCity(): ResourceCollection
    {
        return $this->service->getAllCity();
    }

    public function getCityByName(CityByNameRequest $request)
    {
        return $this->service->getCityByName($request->name);
    }

    public function getAllWarehouses(GetAllWarehousesRequest $request): ResourceCollection
    {
        return $this->service->getAllWarehouses($request->toArray());
    }
}
