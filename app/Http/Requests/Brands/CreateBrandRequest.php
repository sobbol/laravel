<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\Brands;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => ['required', 'string', 'min:3', 'max:50', Rule::unique('brands', 'name')],
            'meta_title'       => 'required|string',
            'meta_description' => 'required|string',
            'description'      => 'required|string',
            'annotation'       => 'required|string',
            'image'            => 'required|mimes:jpg,png,jpeg|max:118048',
            'visible'          => 'required|boolean',
            'position'         => 'required|integer',
        ];
    }
}
