<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\Brands;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'               => ['required', 'string', 'exists:brands,id'],
            'name'             => ['required', 'string', 'min:3', 'max:50'],
            'url'              => ['required', 'string', 'min:3', 'max:50'],
            'meta_title'       => 'required|string',
            'meta_description' => 'required|string',
            'description'      => 'required|string',
            'annotation'       => 'required|string',
            'image'            => 'required|mimes:jpg,png,jpeg|max:118048',
            'visible'          => 'required|boolean',
            'position'         => 'required|integer',
        ];
    }
}
