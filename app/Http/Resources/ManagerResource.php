<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed email
 * @property mixed permission
 * @property mixed name
 * @property mixed description
 * @property mixed blocked
 * @property mixed email_verified_at
 * @property mixed updated_at
 * @property mixed created_at
 */
class ManagerResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */

    public function toArray($request) : array
    {
        return [
            'id'                => $this->id,
            'email'             => $this->email,
            'permission'        => $this->permission,
            'name'              => $this->name,
            "description"       => $this->description,
            'blocked'           => $this->blocked,
            'email_verified_at' => Carbon::parse($this->email_verified_at)->format("Y-m-d H:i:s"),
            'updated_at'        => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'        => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
        ];
    }
}
