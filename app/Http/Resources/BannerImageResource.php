<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed banner_id
 * @property mixed name
 * @property mixed alt
 * @property mixed title
 * @property mixed description
 * @property mixed link
 * @property mixed image
 * @property mixed position
 * @property mixed visible
 * @property mixed updated_at
 * @property mixed created_at
 * @method getCategoryAttribute(mixed $banner_id)
 */
class BannerImageResource extends JsonResource
{

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'alt'         => $this->alt,
            'title'       => $this->title,
            "description" => $this->description,
            'link'        => $this->link,
            'image'       => $this->image,
            'position'    => $this->position,
            'visible'     => $this->visible,
            'updated_at'  => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'  => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
            'banner'      => $this->getBannerAttribute($this->banner_id),
        ];
    }
}
