<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'delivery_id',
        'payment_id',
        'paid',
        'date_paid',
        'user_id',
        'status_id',
        'name',
        'lastname',
        'city',
        'address',
        'phone',
        'email',
        'np_office',
        'ttn',
        'comment',
        'ip',
        'url',
        'details',
        'total_price',
        'discount',
        'coupon_discount',
        'coupon_code',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'delivery_id',
        'payment_id',
        'paid',
        'date_paid',
        'user_id',
        'status_id',
        'name',
        'lastname',
        'city',
        'address',
        'phone',
        'email',
        'np_office',
        'ttn',
        'comment',
        'ip',
        'url',
        'details',
        'total_price',
        'discount',
        'coupon_discount',
        'coupon_code',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [];

    protected $casts = [
        'date_paid'  => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
