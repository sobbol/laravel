<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'code',
        'expire',
        'type',
        'value',
        'order_price',
        'single',
        'usages',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'code',
        'expire',
        'type',
        'value',
        'order_price',
        'single',
        'usages',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [];

    protected $casts = [
        'expire'     => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
