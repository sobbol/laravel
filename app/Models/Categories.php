<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'meta_title',
        'meta_description',
        'description',
        'annotation',
        'url',
        'image',
        'visible',
        'position',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'parent_id',
        'name',
        'meta_title',
        'meta_description',
        'description',
        'annotation',
        'url',
        'image',
        'visible',
        'position',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
