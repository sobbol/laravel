<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BannerImage extends Model
{
    use HasFactory, UuidTrait;

    protected $table = 'banner_image';

    protected $fillable = [
        'id',
        'banner_id',
        'name',
        'alt',
        'title',
        'description',
        'link',
        'image',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'banner',
        'name',
        'alt',
        'title',
        'description',
        'link',
        'image',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $appends = [
        'banner'
    ];

    public function getBannerAttribute()
    {
        return Cache::remember('banner_' . $this->getAttribute('banner_id'), 3500, function () {
            return $this->hasOne(Banner::class, 'id', 'banner_id')->get()->toArray();
        });
    }

    public function scopeDesc($query)
    {
        return $query->orderByDesc('updated_at');
    }

}
