<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Brands extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'name',
        'url',
        'meta_title',
        'meta_description',
        'description',
        'annotation',
        'image',
        'visible',
        'position',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'name',
        'url',
        'meta_title',
        'meta_description',
        'description',
        'annotation',
        'image',
        'visible',
        'position',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function scopeDesc($query)
    {
        return $query->orderByDesc('updated_at');
    }

    protected static function booting(): void
    {
        parent::boot();

        static::created(function ($brand) {
            $brand->url = Str::of($brand->name)->slug('_');
            $brand->save();
        });
    }
}
