<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'email',
        'test',
        'type',
        'approved',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'parent_id',
        'name',
        'email',
        'test',
        'type',
        'approved',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
