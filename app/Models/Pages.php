<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'url',
        'name',
        'meta_title',
        'meta_description',
        'menu_id',
        'icon',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'url',
        'name',
        'meta_title',
        'meta_description',
        'menu_id',
        'icon',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
