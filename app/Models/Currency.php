<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'name',
        'sign',
        'code',
        'rate_from',
        'rate_to',
        'cents',
        'position',
        'enabled'
    ];

    protected $visible = [
        'id',
        'name',
        'sign',
        'code',
        'rate_from',
        'rate_to',
        'cents',
        'position',
        'enabled'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

}
