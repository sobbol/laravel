<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'service',
        'name',
        'description',
        'currency_id',
        'settings',
        'image',
        'position',
        'enabled',
        'position',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'service',
        'name',
        'description',
        'currency_id',
        'settings',
        'image',
        'position',
        'enabled',
        'position',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];
}
