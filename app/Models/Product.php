<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'brand_id',
        'url',
        'name',
        'meta_title',
        'meta_description',
        'annotation',
        'description',
        'body',
        'video',
        'attachment_file',
        'delivery',
        'provider',
        'featured',
        'likes',
        'views',
        'votes',
        'rating',
        'external_id',
        'position',
        'enabled',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'brand_id',
        'url',
        'name',
        'meta_title',
        'meta_description',
        'annotation',
        'description',
        'body',
        'video',
        'attachment_file',
        'delivery',
        'provider',
        'featured',
        'likes',
        'views',
        'votes',
        'rating',
        'external_id',
        'position',
        'enabled',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected static function booting(): void
    {
        parent::boot();

        static::created(function ($product) {
            $product->url = Str::of($product->name)->slug('_');
            $product->save();
        });
    }
}
