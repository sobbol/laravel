<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Observers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class BrandObserver
{
    public function created(): void
    {
        for ($i = 1; $i <= 10; $i++)
        {
            Cache::forget('brands_' . $i);
        }
    }

    public function updated(): void
    {
        for ($i = 1; $i <= 10; $i++)
        {
            Cache::forget('brands_' . $i);
        }
    }
}
