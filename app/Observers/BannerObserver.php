<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Observers;

use Illuminate\Support\Facades\Cache;

class BannerObserver
{
    public function created(): void
    {
        for ($i = 1; $i <= 10; $i++)
        {
            Cache::forget('banners_' . $i);
        }
    }

    public function updated(): void
    {
        for ($i = 1; $i <= 10; $i++)
        {
            Cache::forget('banners_' . $i);
        }
    }
}
