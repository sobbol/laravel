<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\BannerImagesController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.jwt')->group(function () {
    Route::post('/create', [BannerImagesController::class, 'createImage']);
    Route::post('/update', [BannerImagesController::class, 'updateImage']);
    Route::get('/all', [BannerImagesController::class, 'getAllImages']);
    Route::get('/{id}', [BannerImagesController::class, 'getImageId']);
});
