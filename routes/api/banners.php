<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\BannerController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.jwt')->group(function () {
    Route::post('/create', [BannerController::class, 'createBanner']);
    Route::post('/update', [BannerController::class, 'updateBanner']);
    Route::get('/all', [BannerController::class, 'getAllBanners']);
    Route::get('/{id}', [BannerController::class, 'getBannerId']);
});
