<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\BrandsController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.jwt')->group(function () {
    Route::post('/create', [BrandsController::class, 'createBrand']);
    Route::post('/update', [BrandsController::class, 'updateBrand']);
    Route::get('/all', [BrandsController::class, 'getAllBrands']);
    Route::get('/{id}', [BrandsController::class, 'getBrandId']);
});
