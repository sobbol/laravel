<?php
/*
 * Copyright © 2022
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\NovaposhtaController;
use Illuminate\Support\Facades\Route;


Route::prefix('city')->group(function () {
    Route::get('/', [NovaposhtaController::class, 'getAllCity']);
    Route::post('/name', [NovaposhtaController::class, 'getCityByName']);
});

Route::prefix('warehouse')->group(function () {
    Route::get('/', [NovaposhtaController::class, 'getAllWarehouses']);
});

