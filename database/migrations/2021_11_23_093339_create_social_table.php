<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('date_click')->nullable();
            $table->string('facebook')->nullable();
            $table->string('messenger')->nullable();
            $table->string('telegram')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('viber')->nullable();
            $table->string('skype')->nullable();
            $table->string('twitter')->nullable();
            $table->string('vk')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('tumblr')->nullable();
            $table->string('instapaper')->nullable();
            $table->string('refind')->nullable();
            $table->string('mix')->nullable();
            $table->string('bibsonomy')->nullable();
            $table->string('odnoklassniki')->nullable();
            $table->string('pinterest')->nullable();
            $table->string('reddit')->nullable();
            $table->string('google')->nullable();
            $table->string('flipboard')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social');
    }
}
