<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(GroupsTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(BannerImagesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(CallbacksTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CouponsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(DeliveriesTableSeeder::class);
        $this->call(DeliveryPaymentTableSeeder::class);
        $this->call(LabelsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(ManagersTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FeedbacksTableSeeder::class);
    }
}
