<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $faker = Faker::create('en_EN');

        $currencies = [
            [
                'id'         => $faker->uuid,
                'name'       => 'Гривны',
                'sign'       => '₴',
                'code'       => 'UAH',
                'rate_from'  => '1',
                'rate_to'    => '1',
                'cents'      => 2,
                'position'   => 1,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => $faker->uuid,
                'name'       => 'Euro',
                'sign'       => '€',
                'code'       => 'EUR',
                'rate_from'  => '1',
                'rate_to'    => '31.25',
                'cents'      => 2,
                'position'   => 2,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => $faker->uuid,
                'name'       => 'Dollar',
                'sign'       => '$',
                'code'       => 'USD',
                'rate_from'  => '1',
                'rate_to'    => '27.52',
                'cents'      => 2,
                'position'   => 3,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
        ];

        DB::table('currencies')->insert($currencies);
    }
}

