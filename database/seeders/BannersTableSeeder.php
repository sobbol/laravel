<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BannersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('ru_RU');

        $banners = [
            [
                'id'          => $faker->uuid,
                'page'        => 'home',
                'name'        => 'Головна сторінка',
                'description' => 'Банер в шапці на головній сторінці',
                'position'    => '1',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'blog',
                'name'        => 'Сторінка блогу',
                'description' => 'Банер в шапці на сторінці блогу',
                'position'    => '2',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'news',
                'name'        => 'Сторінка новин',
                'description' => 'Банер в шапці на сторінці новин',
                'position'    => '3',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'category',
                'name'        => 'Сторінка категорії',
                'description' => 'Банер в шапці на сторінці категорії продуктів',
                'position'    => '4',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'menu_blog',
                'name'        => 'Бокове меню блогу',
                'description' => 'Банер в боковому меню на сторінці блогу',
                'position'    => '5',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
        ];

        DB::table('banners')->insert($banners);

    }
}

