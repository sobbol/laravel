<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use App\Models\Delivery;
use App\Models\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DeliveryPaymentTableSeeder extends Seeder
{
    public function run()
    {
        $deliveries = $this->getAllDelivery();
        $payments   = $this->getAllPayment();
        $data       = [];

        foreach ($deliveries as $delivery) {
            foreach ($payments as $payment) {
                $data[] = [
                    'id'          => Str::uuid(),
                    'delivery_id' => $delivery,
                    'payment_id'  => $payment,
                    'updated_at'  => now(),
                    'created_at'  => now(),
                ];
            }
        }

        DB::table('delivery_payment')->insert($data);
    }

    public function getAllDelivery(): array
    {
        return Delivery::all()->pluck('id')->toArray();
    }

    public function getAllPayment(): array
    {
        return Payment::all()->pluck('id')->toArray();
    }
}

