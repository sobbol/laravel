<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ManagersTableSeeder extends Seeder
{

    public function run()
    {

        $managers = [
            [
                'id'                => Str::uuid(),
                'email'             => config('install.email'),
                'password'          => Hash::make(config('install.password')),
                'permission'        => 'all',
                'name'              => 'Admin',
                'description'       => 'Адмін на період розробки',
                'blocked'           => false,
                'email_verified_at' => now(),
                'remember_token'    => null,
                'updated_at'        => now(),
                'created_at'        => now(),
            ],
        ];

        DB::table('managers')->insert($managers);

    }
}

