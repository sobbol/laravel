<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MenusTableSeeder extends Seeder
{
    public function run()
    {
        $menus = [
            [
                'id'          => Str::uuid(),
                'name'        => 'Главное меню',
                'position'    => '1',
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => Str::uuid(),
                'name'        => 'Боковое меню',
                'position'    => '2',
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => Str::uuid(),
                'name'        => 'Внизу сайта',
                'position'    => '3',
                'updated_at'  => now(),
                'created_at'  => now(),
            ]
        ];

        DB::table('menus')->insert($menus);

    }
}

