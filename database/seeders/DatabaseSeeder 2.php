<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(BannerSeeder::class);
        $this->call(BannerImageSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(CallbackSeeder::class);
        $this->call(CategorieSeeder::class);
        $this->call(CouponSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(DeliverySeeder::class);
        $this->call(DeliveryPaymentSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FeedbackSeeder::class);

    }
}
