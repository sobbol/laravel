<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $currencies = [
            [
                'id'         => Str::uuid(),
                'name'       => 'Гривня',
                'sign'       => '₴',
                'code'       => 'UAH',
                'rate_from'  => '1',
                'rate_to'    => '1',
                'cents'      => 2,
                'position'   => 1,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => Str::uuid(),
                'name'       => 'Euro',
                'sign'       => '€',
                'code'       => 'EUR',
                'rate_from'  => '1',
                'rate_to'    => '31.25',
                'cents'      => 2,
                'position'   => 2,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => Str::uuid(),
                'name'       => 'Dollar',
                'sign'       => '$',
                'code'       => 'USD',
                'rate_from'  => '1',
                'rate_to'    => '27.52',
                'cents'      => 2,
                'position'   => 3,
                'enabled'    => true,
                'updated_at' => now(),
                'created_at' => now(),
            ],
        ];

        DB::table('currencies')->insert($currencies);
    }

}
