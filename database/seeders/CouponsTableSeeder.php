<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Exception;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CouponsTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $coupons = [];
        $faker   = Faker::create('en_EN');

        for ($i = 1; $i <= 50; $i++) {
            $coupons[] = [
                'id'          => $faker->uuid,
                'code'        => $faker->swiftBicNumber,
                'expire'      => now()->addDays(15),
                'type'        => Arr::random(['absolute', 'percentage']),
                'value'       => random_int(1, 50) . '.' . random_int(1, 99),
                'order_price' => random_int(50, 500) . '.' . random_int(1, 99),
                'single'      => Arr::random([true, false]),
                'usages'      => random_int(1, 10),
                'updated_at'  => now()->addMinutes($i),
                'created_at'  => now()->addMinutes($i),
            ];
        }

        DB::table('coupons')->insert($coupons);
    }
}

