<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $password = Hash::make('!123456Qwerty');
        $faker    = Faker::create('uk_UA');
        $group    = Group::all()->pluck('id')->toArray();

        return [
            'id'                => $faker->uuid,
            'group_id'          => Arr::random($group),
            'name'              => $faker->name,
            'email'             => $faker->email,
            'email_verified_at' => now()->addMinutes(),
            'phone'             => $faker->phoneNumber,
            'photo'             => $faker->imageUrl('200', '200'),
            'address'           => $faker->address,
            'city'              => $faker->city,
            'last_ip'           => $faker->ipv4,
            'password'          => $password,
            'baned'             => false,
            'remember_token'    => null,
            'updated_at'        => now()->addMinutes(),
            'created_at'        => now()->addMinutes(),
        ];
    }
}
