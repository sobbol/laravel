<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $faker = Faker::create();
        $name  = $faker->sentence($nbWords = 2, $variableNbWords = true);

        return [
            'id'               => $faker->uuid,
            'name'             => $name,
            'url'              => Str::of($name)->slug('_'),
            'meta_title'       => $faker->text($maxNbChars = 50),
            'meta_description' => $faker->text($maxNbChars = 90),
            'title'            => $faker->text($maxNbChars = 50),
            'description'      => $faker->text($maxNbChars = 2000),
            'annotation'       => $faker->text($maxNbChars = 500),
            'image'            => $faker->imageUrl($width = 800, $height = 600),
            'type'             => Arr::random(['blog', 'news']),
            'visible'          => true,
            'updated_at'       => now()->addMinutes(),
            'created_at'       => now()->addMinutes(),
        ];
    }
}
