<?php



return [
    'page' => env('DEFAULT_PAGINATION_PAGE', 1),
    'per_page' => env('DEFAULT_PAGINATION_PER_PAGE', 15)
];
