<?php
/*
 * Copyright © 2023
 * Author: Serhii Sobol
 * GitLab:https://gitlab.com/sobbol
 */

return [

    'national_bank' => env('NATIONAL_BANK_PATH_COURSE', 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'),

];
